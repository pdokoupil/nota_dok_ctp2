function getInfo()
	return {
		tooltip = "Unload units from a transporter",
        onNoUnits = SUCCESS,
        parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "positionToUnload", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting

function Run(self, units, parameter)

	local transportId = parameter.transporter
    local positionToUnload = parameter.positionToUnload

    local modifier = {"shift"}
    
    local tbl = SpringGetUnitIsTransporting(transportId)
    for i = 1, #tbl do
        SpringGiveOrderToUnit(transportId, CMD.UNLOAD_UNITS, {positionToUnload.x, positionToUnload.y, positionToUnload.z, 60}, modifier)
        return RUNNING
    end
    return SUCCESS
end

function Reset(self)
    return self
end