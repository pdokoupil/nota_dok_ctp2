function getInfo()
	return {
		tooltip = "Load units to transporter",
        onNoUnits = SUCCESS,
        parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitsToLoad", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting

function tableContainsValue(tbl, value)
    for i = 1, #tbl do
        if tbl[i] == value then
            return true
        end
    end
    return false
end

function Run(self, units, parameter)

	local transportId = parameter.transporter
    local transportedIds = parameter.unitsToLoad

    local modifier = {}
    
    for tKey, tId in pairs(transportedIds) do	
        local tbl = SpringGetUnitIsTransporting(transportId)
        if tableContainsValue(tbl, tId) == false then
            -- local pointX, pointY, pointZ = Spring.GetUnitPosition(tId)	
            SpringGiveOrderToUnit(transportId, CMD.LOAD_UNITS, {tId}, modifier)
            return RUNNING
        end
    end

    return SUCCESS
end

function Reset(self)
    return self
end