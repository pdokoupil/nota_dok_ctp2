local sensorInfo = {
	name = "getHillGroups",
	desc = "Divide enemies into groups for capturing the hills",
	author = "dok",
	date = "2020-05-21",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local GetGroundOrigHeight = Spring.GetGroundOrigHeight
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetTeamUnits = Spring.GetTeamUnits

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return groups for capturing the hills
return function()
    
    -- We have four hills so we create four groups
    -- On the fourth hill there is an enemy that should be destroyed
    local groups = {}
    groups[4] = {}
    sizeOfFourth = 0

    local myTeamId = SpringGetMyTeamID()
    local teamUnits = SpringGetTeamUnits(myTeamId)
    local sizes = {}

    local armpwIndex = 1
    
    for key, value in ipairs(teamUnits) do
        local defId = SpringGetUnitDefID(value)
        local unitName = UnitDefs[defId].name
        if unitName == "armpw" then
            if groups[armpwIndex] == nil then
                groups[armpwIndex] = {}
            end
            groups[armpwIndex][value] = 1
            armpwIndex = armpwIndex + 1
        elseif unitName == "armthovr" then
            groups[4][value] = sizeOfFourth + 1
            sizeOfFourth = sizeOfFourth + 1
        end
    end

    return groups

end