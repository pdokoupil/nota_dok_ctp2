local sensorInfo = {
	name = "getHills",
	desc = "Get positions of hills",
	author = "dok",
	date = "2020-05-21",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local GetGroundOrigHeight = Spring.GetGroundOrigHeight

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return points on hills
return function(hillHeight)
    local mapWidth = Game.mapSizeX
    local mapHeight = Game.mapSizeZ
    
    local tileWidth = 256
    local tileHeight = 256

    local i = 1
    local hills = {}

    for x = 0, mapWidth, tileWidth / 2 do
        for y = 0, mapHeight, tileHeight / 2 do
            local groundHeight = GetGroundOrigHeight(x, y)
            if groundHeight >= hillHeight then
                hills[i] = Vec3(x, groundHeight, y)
                i = i + 1
            end
        end
    end

    return hills

end