local sensorInfo = {
	name = "getUnitsTable",
	desc = "Get table of units",
	author = "dok",
	date = "2020-05-21",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local GetGroundOrigHeight = Spring.GetGroundOrigHeight
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetTeamUnits = Spring.GetTeamUnits

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return table of units
return function()
    
    local groups = {}

    local myTeamId = SpringGetMyTeamID()
    local teamUnits = SpringGetTeamUnits(myTeamId)
    
    for key, value in ipairs(teamUnits) do
        local defId = SpringGetUnitDefID(value)
        local unitName = UnitDefs[defId].name
        if  groups[unitName] == nil then
            groups[unitName] = { value }
        else
            table.insert(groups[unitName], value)
        end
    end

    return groups

end