local sensorInfo = {
	name = "getBearId",
	desc = "Get id of the Bear unit",
	author = "dok",
	date = "2020-05-21",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetTeamUnits = Spring.GetTeamUnits

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return id of the bear
return function()
    
    local myTeamId = SpringGetMyTeamID()
    local teamUnits = SpringGetTeamUnits(myTeamId)
    local bearId = 0

    for key, value in ipairs(teamUnits) do
        local defId = SpringGetUnitDefID(value)
        local unitName = UnitDefs[defId].name
        if unitName == 'armthovr' then
            return value
        end
    end

    -- my team does not have any bear 
    return bearId

end